//
//  SelectServiceVC.m
//  TaxiNow Driver
//
//  Created by My Mac on 3/23/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.

#import "SelectServiceVC.h"
#import "SelectServiceCell.h"
#import <CoreText/CoreText.h>

@interface SelectServiceVC ()
{
    NSMutableArray *arr;
    NSMutableArray *arrType;
    NSMutableArray *arrPricet;
    BOOL isContain;
    NSMutableArray *arrWtype;
    NSMutableArray *arrWtypePrice;
    NSInteger index;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
}
@end

@implementation SelectServiceVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setBackBarItem];
    arrType=[[NSMutableArray alloc]init];
    arr=[[NSMutableArray alloc] initWithObjects:@"one",@"two",@"three", nil];
    arrWtype=[[NSMutableArray alloc]init];
    arrWtypePrice=[[NSMutableArray alloc]init];
    arrPricet=[[NSMutableArray alloc]init];
    NSLog(@"register details :%@",self.dictparam);
    NSLog(@"image  :%@",self.imgP);
    [APPDELEGATE hideLoadingView];
    
    self.tblselectService.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if([self.strFromRegister isEqualToString:@"fromRegister"])
    {
        self.btnRegister.tag=1;
        [self getType];
        [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"SELECR_SERVICE", nil) forState:UIControlStateHighlighted];
        //self.btnMenu.frame = CGRectMake(self.btnMenu.frame.origin.x, self.btnMenu.frame.origin.y, 125, self.btnMenu.frame.size.height);
    }
    else
    {
        [self.btnRegister setTitle:NSLocalizedString(@"Update", nil) forState:UIControlStateNormal];
        [self.btnRegister setTitle:NSLocalizedString(@"Update", nil) forState:UIControlStateHighlighted];
        [self getSelectedTypes];
        [self.btnMenu setTitle:NSLocalizedString(@"EDIT_SERVICES", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"EDIT_SERVICES", nil) forState:UIControlStateHighlighted];
        //self.btnMenu.frame = CGRectMake(self.btnMenu.frame.origin.x, self.btnMenu.frame.origin.y, 125, self.btnMenu.frame.size.height);
    }
    
    self.btnServiceOffer.titleLabel.text = NSLocalizedString(@"SERVICE_OFFER", nil);
    self.lblServiceOffer1.text = NSLocalizedString(@"SERVICE_OFFER1", nil);
    self.btnAverageOffer.titleLabel.text = NSLocalizedString(@"AVERAGE_OFFER", nil);
    self.lblAveragePrice1.text = NSLocalizedString(@"AVERAGE_OFFER1", nil);
}
-(void)viewWillAppear:(BOOL)animated
{
    self.lblSelectService.text = NSLocalizedString(@"SELECR_SERVICE", nil);
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SELECR_SERVICE", nil)];
    
    [self.btnSelectService setAttributedTitle:string forState:UIControlStateNormal];
    [self.btnSelectService setAttributedTitle:string forState:UIControlStateHighlighted];
    
    //self.lblLine.frame = CGRectMake(self.lblSelectService.frame.size.height+5, self.lblSelectService.frame.origin.y, self.lblSelectService.frame.size.width, 1);
}
-(void)getType
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_WALKER_TYPE withParamData:nil withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];if([[response valueForKey:@"success"] intValue]==1)
             {
                 [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
                 arrType=[response valueForKey:@"types"];
                 NSLog(@"arr :%@",arrType);
                 [arrPricet removeAllObjects];
                 [arrWtype removeAllObjects];
                 for(int i=0;i<arrType.count;i++)
                 {
                     [arrPricet addObject:@""];
                     [arrWtype addObject:@""];
                 }
                 [self.tblselectService reloadData];
                 NSLog(@"Check %@",response);
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
             }
         }
         
     }];
}

-(void)getSelectedTypes
{
    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    [dictparam setObject:strUserId forKey:PARAM_ID];
    [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_GET_SELECTED_TYPES withParamData:dictparam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             response=[[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
             if([[response valueForKey:@"success"] boolValue])
             {
                 arrType=[response valueForKey:@"types"];
                 NSLog(@"arr :%@",arrType);
                 [arrPricet removeAllObjects];
                 [arrWtype removeAllObjects];
                 for(int i=0;i<arrType.count;i++)
                 {
                     [arrPricet addObject:@""];
                     [arrWtype addObject:@""];
                 }
                 [self.tblselectService reloadData];
                 NSLog(@"Check %@",response);
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }
     }];

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    #define ACCEPTABLE_CHARACTERS @"1234567890"
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *txtf=(UITextField *)textField;
    NSIndexPath *path = [NSIndexPath indexPathForRow:txtf.tag inSection:0];
    NSDictionary *dictType=[arrType objectAtIndex:txtf.tag];
    [textField resignFirstResponder];
    //if(![textField.text isEqualToString:[NSString stringWithFormat:@"%@",[dictType valueForKey:@"base_price"]]])
    //{
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    
    NSString *strTypeId=[NSMutableString stringWithFormat:@"%@",[dictType valueForKey:@"id"]];
    cell.txtprice.text = textField.text;
    /*if(![arrWtype containsObject:strTypeId])
     [arrWtype addObject:strTypeId];*/
    NSLog(@"index  :%ld",(long)txtf.tag);
    [arrPricet replaceObjectAtIndex:path.row withObject:cell.txtprice.text];
    NSLog(@"log :%@",arrPricet);
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //}
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    SelectServiceCell *cell=(SelectServiceCell *)[self.tblselectService dequeueReusableCellWithIdentifier:@"PriceCell"];
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.tblselectService];
    
    NSIndexPath * indexPath = [self.tblselectService indexPathForRowAtPoint:point];
    
    NSLog(@"indexPath = %ld",(long)indexPath.row);
    
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-(30*(int)indexPath.row), self.view.frame.size.width, self.view.frame.size.height);
}

#pragma mark tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrType.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
    SelectServiceCell *cell=(SelectServiceCell *)[tableView dequeueReusableCellWithIdentifier:@"PriceCell"];
    
    if([self.strFromRegister isEqualToString:@"fromRegister"])
    {
        cell.lbltitle.text=[dict valueForKey:@"name"];
        cell.txtprice.tag=indexPath.row;
        cell.txtprice.enabled=NO;
        return cell;
    }
    else
    {
        cell.lbltitle.text=[dict valueForKey:@"name"];
        cell.txtprice.tag=indexPath.row;
        if([[dict valueForKey:@"is_selected"] boolValue])
        {
            cell.txtprice.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"walker_base_price"]];
            cell.imgCheck.image=[UIImage imageNamed:@"service_check"];
            cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
            cell.txtprice.enabled=YES;
            cell.txtdoller.hidden=NO;
            NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
            NSString *strTypeId=[NSString stringWithFormat:@"%@",[dictType valueForKey:@"id"]];
            NSString *strTypeBasePrice=[NSString stringWithFormat:@"%@",[dictType valueForKey:@"base_price"]];
            NSLog(@"selected id : %@",strTypeId);
            [arrWtype replaceObjectAtIndex:indexPath.row withObject:strTypeId];
            [arrPricet replaceObjectAtIndex:indexPath.row withObject:cell.txtprice.text];
            
            NSLog(@"price = %@",arrPricet);
            NSLog(@"types = %@",arrWtype);
        }
        else
        {
            
        }
        return cell;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectServiceCell *Cell=(SelectServiceCell *)[self.tblselectService cellForRowAtIndexPath:indexPath];
    if([Cell.imgCheck.image isEqual:[UIImage imageNamed:@"un_checkbox"]])
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"service_check"];
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
        Cell.txtprice.enabled=YES;
        [Cell.txtprice becomeFirstResponder];
        Cell.txtdoller.hidden=NO;
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        NSString *strTypeBasePrice=[dictType valueForKey:@"base_price"];
        Cell.txtprice.text=[NSString stringWithFormat:@"%@",strTypeBasePrice];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrWtype replaceObjectAtIndex:indexPath.row withObject:strTypeId];
        //[arrPricet addObject:strTypeBasePrice];
    }
    else
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"un_checkbox"];
        Cell.imgs.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.txtprice.enabled=NO;
        Cell.txtdoller.hidden=NO;
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
        
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        NSString *strTypeBasePrice=[dictType valueForKey:@"base_price"];
        NSLog(@"dict deselect : %@",strTypeId);
        
        [arrWtype replaceObjectAtIndex:indexPath.row withObject:@""];
        [arrPricet replaceObjectAtIndex:indexPath.row withObject:@""];
        Cell.txtprice.text = @"";
    }
    [self.tblselectService deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)btnCommentClick:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    NSLog(@"text :%@",cell.lbltitle.text);
    
    // cell.btnPrice.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_price"]];
    
    //[self.tblselectService reloadData];
    
    /*
     UIButton *senderButton = (UIButton *)sender;
     
     NSLog(@"current Row=%ld",(long)senderButton.tag);
     // NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
     NSLog(@" sdsd %@" ,senderButton.titleLabel.text);
     */
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickforRegister:(id)sender
{
    [self.view endEditing:YES];
    int value=0;
    
    NSLog(@"price = %@",arrPricet);
    NSLog(@"types = %@",arrWtype);
    
    for(int i=0;i<arrWtype.count;i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",[arrWtype objectAtIndex:i]];
        if([str isEqualToString:@""])
        {
            value++;
        }
    }
    
    if(value==arrWtype.count)
    {
        [APPDELEGATE showToastMessage:@"Please select service."];
    }
    else
    {
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        for(int i=0;i<arrPricet.count;i++)
        {
            int val=[[arrPricet objectAtIndex:i] intValue];
            if(val<=0)
            {
                [arrPricet removeObject:[arrPricet objectAtIndex:i]];
            }
        }
        
        for(int i=0;i<arrWtype.count;i++)
        {
            int val=[[arrWtype objectAtIndex:i] intValue];
            if(val<=0)
            {
                [arrWtype removeObject:[arrWtype objectAtIndex:i]];
            }
        }
        
        NSString *joinedStringprice11 = [arrPricet componentsJoinedByString:@","];
        NSString *joinedStringprice12 = [arrWtype componentsJoinedByString:@","];
        
        NSLog(@"price2   :%@",joinedStringprice11);
        NSLog(@"type    :%@",joinedStringprice12);
        
        if(self.btnRegister.tag==1)
        {
            [self.dictparam setValue:joinedStringprice12 forKey:PARAM_WALKER_TYPE];
            [self.dictparam setValue:joinedStringprice11 forKey:PARAM_WALKER_PRICE];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            [afn getDataFromPath:FILE_REGISTER withParamDataImage:self.dictparam andImage:self.imgP withBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:(NSLocalizedString(@"REGISTER_SUCCESS", nil))];
                         arrUser=response;
                         NSLog(@"res :%@",response);
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 NSLog(@"REGISTER RESPONSE --> %@",response);
             }];
        }
        else
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            
            [dict setValue:joinedStringprice12 forKey:PARAM_WALKER_TYPE];
            [dict setValue:joinedStringprice11 forKey:@"base_price"];
            [dict setObject:strUserId forKey:PARAM_ID];
            [dict setObject:strUserToken forKey:PARAM_TOKEN];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            
            [afn getDataFromPath:FILE_EDIT_TYPES withParamData:dict withBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         //[APPDELEGATE showToastMessage:(NSLocalizedString(@"REGISTER_SUCCESS", nil))];
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 NSLog(@"EDIT SERIVCES --> %@",response);
             }];
        }
    }
}

@end